# embedded-system-project

This is a project I did with a classmate during my third year at my engineering school.
The goal was to control a small car with our phone thanks to Bluetooth.

*Electronic part (with the software Altium):
1) positionning the components on our printed circuit board (PCB);
2) PCB routing;
3) printing of the card.

*Programming part:
1) coding our card with SystemWorkbench for STM32;
2) coding an app to control the car with our phone (with AndroidStudio).

For the programming part, we used the following website to understand how to create an Android app that control the HC05 Bluetooth device:
https://www.instructables.com/Android-Bluetooth-Control-LED-Part-2/

<br><br>
To program our PCB, we used a state machine:

<div align="center">
  <img src="./images_readme/state_machine.png" width="800">
</div>
<br><br><br>

The following activity diagram shows how the phone app works:
<div align="center">
  <img src="./images_readme/activity_diagram_app.png">
</div>
<br><br>

Here is how it looks (some characters are sent to the PCB when we press the buttons):

<div align="center">
  <img src="./images_readme/pse_app.png">
</div>
